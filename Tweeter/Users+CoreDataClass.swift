//
//  Users+CoreDataClass.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 6/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit

@objc(Users)
public class Users: NSManagedObject {
    
    class func insertOrExtractUserWithInfo(_ insertExtractNickName: String!, insertThumbnailName: String? = nil, saveInsertedSurveyInfo: Bool? = true) -> Users? {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext!
        
        //To fetch observation from entity and so as to choose observation to output, delete, or update
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        //to rectify issue of displaying results
        fetchRequest.returnsObjectsAsFaults = false
        
        fetchRequest.predicate = NSPredicate(format: "nickName = %@", insertExtractNickName)
        
        //For extraction
        if let user = (try? context.fetch(fetchRequest))?.first as? Users{
            return user
        }
            
        //For insertion
        else if let user = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context) as? Users{
            
            user.nickName = insertExtractNickName
            user.favourite = false
            user.thumbnailName = insertThumbnailName
            
            if saveInsertedSurveyInfo! {
                do {
                    try context.save()
                } catch _ {
                }
            }
            
            return user
        }
        return nil
    }
    
    class func fetchFavouritesRequest(sortAscending: Bool? = nil) -> [Users]? {
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        fetchRequest.predicate = NSPredicate(format: "favourite = YES")
        if let sortAscendingTest = sortAscending {
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "nickName", ascending: sortAscendingTest)]
        }
        
        if let fetchedResults = (try? context.fetch(fetchRequest)) as? [Users] {
            return fetchedResults
        }
        
        return nil
        
    }
    
    class func fetchAllRequest(sortAscending: Bool? = nil) -> [Users]? {
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        if let sortAscendingTest = sortAscending {
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "nickName", ascending: sortAscendingTest)]
        }
        
        
        if let fetchedResults = (try? context.fetch(fetchRequest)) as? [Users] {
            return fetchedResults
        }
        
        return nil
        
    }
    
    class func fetchLocalUserRequest() -> Users? {
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        fetchRequest.predicate = NSPredicate(format: "nickName = %@", "Calvin")
        
        if let fetchedResults = (try? context.fetch(fetchRequest))?.first as? Users {
            return fetchedResults
        }
        return nil
    }
    
    class func fetchOtherUsersRequest() -> [Users]? {
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        fetchRequest.predicate = NSPredicate(format: "favourite != nil")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "nickName", ascending: true)]
        
        if let fetchedResults = (try? context.fetch(fetchRequest)) as? [Users] {
            return fetchedResults
        }
        return nil
    }
}
