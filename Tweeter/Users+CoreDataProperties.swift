//
//  Users+CoreDataProperties.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 6/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//
//

import Foundation
import CoreData


extension Users {

    @NSManaged public var favourite: NSNumber?
    @NSManaged public var nickName: String?
    @NSManaged public var thumbnailName: String?
    @NSManaged public var twits: NSSet?

}


