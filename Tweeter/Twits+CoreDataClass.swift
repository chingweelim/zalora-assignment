//
//  Twits+CoreDataClass.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 6/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit

@objc(Twits)
public class Twits: NSManagedObject {
    
    class func insertOrExtractTwitWithInfo(_ insertedExtractedNickName: String!, insertedExtractedTwit: String!, saveInsertedSurveyInfo: Bool? = true) -> Twits? {
        
        let appDel = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext!
        
        //To fetch observation from entity and so as to choose observation to output, delete, or update
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Twits")
        
        //to rectify issue of displaying results
        fetchRequest.returnsObjectsAsFaults = false
        
        //to choose attribute, then an observation from Core Data
        let subpredicate1 = NSPredicate(format: "nickName = %@", insertedExtractedNickName)
        let subpredicate2 = NSPredicate(format: "twit = %@", insertedExtractedTwit)
        
        let subPredicates1 = [subpredicate1,subpredicate2]
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: subPredicates1)
        
        
        //For extraction
        if let user = (try? context.fetch(fetchRequest))?.first as? Twits{
            return user
        }
            
        //For insertion
        else if let twit = NSEntityDescription.insertNewObject(forEntityName: "Twits", into: context) as? Twits{
            
            twit.nickName = insertedExtractedNickName!
            twit.timeStamp = NSDate()
            twit.twit = insertedExtractedTwit!
            twit.user = Users.insertOrExtractUserWithInfo(insertedExtractedNickName!)
            if saveInsertedSurveyInfo! {
                do {
                    try context.save()
                } catch _ {
                }
            }
            
            return twit
        }
        return nil
    }
    
    class func fetchUserTwitsRequest(nickName: String!) -> [Twits]? {
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Twits")
        
        //to choose attribute, then an observation from Core Data
        fetchRequest.predicate = NSPredicate(format: "nickName = %@", nickName)
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: false)]
        
        if let fetchedResults = (try? context.fetch(fetchRequest)) as? [Twits] {
            return fetchedResults
        }
        
        return nil
    }
    
    class func fetchFavouriteLatestTwitRequest(attributeToSort: String!) -> [Twits] {
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext!
        
        let users = Users.fetchFavouritesRequest(sortAscending: true)!
        
        var fetchResults = [Twits]()
        for (_,user) in users.enumerated() {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Twits")
            
            //to choose attribute, then an observation from Core Data
            fetchRequest.predicate = NSPredicate(format: "nickName = %@", user.nickName!)
            
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: false)]
            
            if let fetchedResult = (try? context.fetch(fetchRequest))!.first as? Twits {
                fetchResults.append(fetchedResult)
            }
        }
        
        return fetchResults
        
    }
    
    class func fetchAllLatestTwitRequest(attributeToSort: String!) -> [Twits] {
        
        let appDelegate =
            UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext!
        
        let users = Users.fetchAllRequest()!
        
        var fetchResults = [Twits]()
        for (_,user) in users.enumerated() {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Twits")
            
            //to choose attribute, then an observation from Core Data
            fetchRequest.predicate = NSPredicate(format: "nickName = %@", user.nickName!)
            
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timeStamp", ascending: false)]
            
            if let fetchedResult = (try? context.fetch(fetchRequest))!.first as? Twits {
                fetchResults.append(fetchedResult)
            }
        }
        
        return fetchResults
        
    }
    
    class func deleteJSONTwits(_ creationDate: NSDate!, saveContext: Bool? = true) {
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext!
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Twits")
        
        fetchRequest.predicate = NSPredicate(format: "timeStamp = %@", creationDate)
        
        //For deletion
        if let twitsToDelete = (try? context.fetch(fetchRequest)) as? [Twits] {
            
            //to delete
            for (_,twitToDelete) in twitsToDelete.enumerated(){
                context.delete(twitToDelete)
            }
            
            if saveContext! {
                do {
                    try context.save()
                } catch _ {
                }
            }
            
        }
        
    }
}
