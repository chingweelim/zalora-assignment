//
//  HomeViewController.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 3/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {

    @IBOutlet weak var tweetTextView: UITextView!
    @IBOutlet var homeTableView: UITableView!
    
    var fullMessage = ""
    var splitMessages = [String]()
    var localUser: Users!
    var otherUsers: [Users]!
    var selectedUser: Users?
    
    func twitLimit() -> Int {
        return 50
    }
    
    //mimic refresh data using below JSON
    var jSONUsers = [JSONUsers]()
    //set timestamp as 31 Dec 2019 as an example
    func jSONUsersGenerator() {
        let jSONForRefreshString =
        """
            [
                {
                    "nickName": "Johnny",
                    "thumbnailName": "johnny",
                    "twits": [
                    {
                    "twit": "I am very handsome",
                    "timeStamp": "2019-12-31T19:32:00Z"
                    },
                    {
                    "twit": "I am super handsome",
                    "timeStamp": "2019-12-31T19:32:00Z"
                    }
                    ]
                },
                {
                    "nickName": "Jenny",
                    "thumbnailName": "jenny",
                    "twits": [
                    {
                    "twit": "I am very pretty",
                    "timeStamp": "2019-12-31T19:32:00Z"
                    },
                    {
                    "twit": "I am super pretty",
                    "timeStamp": "2019-12-31T19:32:00Z"
                    },
                    {
                    "twit": "I am extremely pretty",
                    "timeStamp": "2019-12-31T19:32:00Z"
                    }
                    ]
                }
            ]
        """
        let jsonData = Data(jSONForRefreshString.utf8)
        do {
            jSONUsers = try JSONDecoder().decode([JSONUsers].self, from: jsonData)
        }catch let jsonErr{
            print("json decoder error", jsonErr)
        }
    }
    
    
    @IBAction func refreshActionButton(_ sender: UIBarButtonItem) {
        
        for (_,jSONUser) in jSONUsers.enumerated() {
            let _ = Users.insertOrExtractUserWithInfo(jSONUser.nickName, insertThumbnailName: jSONUser.thumbnailName, saveInsertedSurveyInfo: false)
            for (_,jSONTwit) in jSONUser.twits.enumerated() {
                let twit = Twits.insertOrExtractTwitWithInfo(jSONUser.nickName, insertedExtractedTwit: jSONTwit.twit, saveInsertedSurveyInfo: false)
                //insert the 31 Dec 2019 date
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssX"
                if let creationDate = dateFormatter.date(from: jSONTwit.timeStamp) {
                    twit!.timeStamp = creationDate as NSDate
                }
            }
        }
        otherUsers = Users.fetchOtherUsersRequest()!
        tableView.reloadData()
    }
    
    @IBAction func favouritesActionButton(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "HomeToFavourite", sender: sender)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addCancelButtonOnKeyboard()
        jSONUsersGenerator()
        prePopulateCoreData()
    }
    
    // MARK: - TableView
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 44.0
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return otherUsers.count
        }
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            return "Me"
        }else{
            return "Fellow Tweeterers"
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeCell") as! HomeUserTableViewCell
        cell.delegate = self
        if indexPath.section == 0 {
            let localUserNickName = localUser.nickName!
            let localUserThumbnail = localUser.thumbnailName!
            cell.nickNameLabel.text = localUserNickName
            cell.thumbnailImageView.image = UIImage(named: localUserThumbnail)
            cell.favouriteButton.alpha = 0.0
            cell.twitLabel.text = Twits.fetchUserTwitsRequest(nickName: localUserNickName)!.first!.twit
            let dateFormatter = DateFormatter()
            let creationDate = Twits.fetchUserTwitsRequest(nickName: localUserNickName)!.first!.timeStamp
            dateFormatter.dateStyle = DateFormatter.Style.full
            cell.dateLabel.text = dateFormatter.string(from: creationDate! as Date)
        }else{
            let otherUserNickName = otherUsers[indexPath.row].nickName!
            let otherUserThumbnail = otherUsers[indexPath.row].thumbnailName!
            let otherUserFavourite = Bool(truncating: otherUsers[indexPath.row].favourite!)
            cell.nickNameLabel.text = otherUserNickName
            cell.thumbnailImageView.image = UIImage(named: otherUserThumbnail)
            cell.favouriteButton.alpha = 1.0
            if otherUserFavourite {
                cell.favouriteButton.setTitle("Favourited", for: .normal)
            }else{
                cell.favouriteButton.setTitle("Favourite", for: .normal)
            }
            cell.twitLabel.text = Twits.fetchUserTwitsRequest(nickName: otherUserNickName)!.first!.twit
            let dateFormatter = DateFormatter()
            let creationDate = Twits.fetchUserTwitsRequest(nickName: otherUserNickName)!.first!.timeStamp
            dateFormatter.dateStyle = DateFormatter.Style.full
            cell.dateLabel.text = dateFormatter.string(from: creationDate! as Date)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            selectedUser = localUser
        }else{
            selectedUser = otherUsers[indexPath.row]
        }
        performSegue(withIdentifier: "HomeToSingleUser", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "HomeToSingleUser" {
            let singleUserViewController = segue.destination as! SingleUserViewController
            singleUserViewController.singleUser = selectedUser
        }else if segue.identifier == "HomeToFavourite" {
            let favouriteUserViewController = segue.destination as! FavouriteUsersViewController
            favouriteUserViewController.favouriteUsers = Users.fetchFavouritesRequest(sortAscending: true)!
        }
    }
    
    
    

}

extension HomeViewController {
    
    //MARK: - splitMessage function
    func splitMessage(message: String) -> [String] {
        var words = message.components(separatedBy: " ")
        var splitMessageArray = [String]()
        var splitMessage = String()
        var splitMessageTest = String()
        var wordIndex = 0
        var splitNumberTotal = 1
        
        //when number of splits reaches a multiple of 10, word count will increase across all message chunks (e.g. 1/9 -> 1/10). Therefore, will need to reset back to square one (SqOne) to recalculate chunks
        var tenMultiple = 10
        var denominatorWordCount = 1
        
        //while loop to create array of splitMessages
        while wordIndex <= words.count - 1 {
            
            //go back to SqOne if number of chunks reaced a multiple of 10
            if splitMessage.count + 1 == tenMultiple {
                tenMultiple = tenMultiple*10
                denominatorWordCount += 1
                
                //settings back to SqOne
                splitMessageArray = [String]()
                splitMessage = String()
                splitMessageTest = String()
                wordIndex = 0
                splitNumberTotal = 1
            //else carry on as usual
            } else {
                if splitMessageTest.count == 0{
                    splitMessageTest = words[wordIndex]
                }else{
                    splitMessageTest = "\(splitMessageTest) \(words[wordIndex])"
                }
                
                //if splitMessage more than 50 characters, append to array original splitMessage and reset settings for next splitMessage
                if splitMessageTest.count + String(splitNumberTotal).count + 2 + denominatorWordCount > twitLimit() {
                    splitMessageArray.append(splitMessage)
                    splitNumberTotal += 1
                    splitMessage = ""
                    splitMessageTest = ""
                    
                //else if reach last word, do last append
                }else if wordIndex == words.count - 1 {
                    splitMessageArray.append(splitMessageTest)
                    wordIndex += 1
                    
                //else append word to splitMessage and move on to next word
                }else{
                    if splitMessage.count == 0{
                        splitMessage = words[wordIndex]
                    }else{
                        splitMessage = "\(splitMessage) \(words[wordIndex])"
                    }
                    wordIndex += 1
                }
            }
        }
        
        //for loop to input numbering of splitMessages
        for (j,_) in splitMessageArray.enumerated() {
            splitMessageArray[j] = "\(j+1)/\(splitNumberTotal) \(splitMessageArray[j])"
        }
        
        return splitMessageArray
    }
    
    func prePopulateCoreData() {
        if !UserDefaults.standard.bool(forKey: "prePopulated") {
            let localUser = Users.insertOrExtractUserWithInfo("Calvin", insertThumbnailName: "calvin", saveInsertedSurveyInfo: false)
            //set favourite to nil for localUser
            localUser!.favourite = nil
            
            let _ = Twits.insertOrExtractTwitWithInfo("Calvin", insertedExtractedTwit: "I am feeling so energetic right now", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Calvin", insertedExtractedTwit: "I am feeling so sleepy right now...", saveInsertedSurveyInfo: false)
            
            let _ = Users.insertOrExtractUserWithInfo("Johnny", insertThumbnailName: "johnny", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Johnny", insertedExtractedTwit: "Just had a break up. Feeling so sad right now", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Johnny", insertedExtractedTwit: "Maybe I should back to work. Break up sobbing is over.", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Johnny", insertedExtractedTwit: "I love my new desk space!", saveInsertedSurveyInfo: false)
            
            let _ = Users.insertOrExtractUserWithInfo("Jenny", insertThumbnailName: "jenny", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Jenny", insertedExtractedTwit: "Just had a break up with Johnny. He did it FIRST!!!", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Jenny", insertedExtractedTwit: "I need a drink.", saveInsertedSurveyInfo: false)
            
            let _ = Users.insertOrExtractUserWithInfo("Peter", insertThumbnailName: "peter", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Peter", insertedExtractedTwit: "Why did my team lose so badly today!!! We were definitely missing something.", saveInsertedSurveyInfo: false)
            
            
            let _ = Users.insertOrExtractUserWithInfo("Mary", insertThumbnailName: "mary", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Mary", insertedExtractedTwit: "Okay okay, stop it with the 'Bloody Mary' Jokes", saveInsertedSurveyInfo: false)
            let _ = Twits.insertOrExtractTwitWithInfo("Mary", insertedExtractedTwit: "Hah, got my A-Game back on.", saveInsertedSurveyInfo: true)
            
            UserDefaults.standard.set(true, forKey: "prePopulated")
        }
        localUser = Users.fetchLocalUserRequest()!
        otherUsers = Users.fetchOtherUsersRequest()!
    }
    
    //add cancel toolbar to keyboard
    func addCancelButtonOnKeyboard() {
        
        let doneToolBar = UIToolbar()
        doneToolBar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: UIBarButtonItem.Style.done, target: self, action: #selector(self.cancelActionButton))
        
        doneToolBar.setItems([flexSpace, done], animated: false)
        
        doneToolBar.sizeToFit()
        
        tweetTextView.inputAccessoryView = doneToolBar
        
    }
    
    @objc func cancelActionButton() {
        
        tweetTextView.resignFirstResponder()
        tweetTextView.text = "Tweet it!"
        tweetTextView.textColor = UIColor.lightGray
        
    }
    
}

extension HomeViewController: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
      
        if textView.text == "Tweet it!" && textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        return true
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
        textView.textColor = UIColor.black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = "Tweet it!"
        textView.textColor = UIColor.lightGray
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let words = textView.text.components(separatedBy: " ")
        if words.last!.count > 50 {
            let alert = UIAlertController(title: "Your word is more than 50 characters", message: "If you intent to type 'supercalifragilisticexpialidocious', you spelt it wrong.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
            //pop up alert
            self.present(alert, animated: true, completion: nil)
            
            return false
        }else if text == "\n" {
            if textView.text.count < twitLimit() {
                fullMessage=textView.text
                let _ = Twits.insertOrExtractTwitWithInfo(localUser.nickName, insertedExtractedTwit: fullMessage, saveInsertedSurveyInfo: true)
            }else{
                splitMessages = splitMessage(message: textView.text)
                for (_,splitMessage) in splitMessages.enumerated() {
                    let _ = Twits.insertOrExtractTwitWithInfo(localUser.nickName, insertedExtractedTwit: splitMessage, saveInsertedSurveyInfo: true)
                }
            }
            tableView.reloadData()
            textView.resignFirstResponder()
            return false
        }else{
            return true
        }
    }
    
}

extension HomeViewController: HomeCellConnectDelegate {
    func favouriteUnfavouriteUser(selectedQuestionButtonOutlet: UIButton!, selectedNickNamelabelOutlet: UILabel!) {
        let user = Users.insertOrExtractUserWithInfo(selectedNickNamelabelOutlet.text)!
        if Bool(truncating: user.favourite!) {
            selectedQuestionButtonOutlet.setTitle("Favourite", for: .normal)
            user.favourite = false
        }else{
            selectedQuestionButtonOutlet.setTitle("Favourited", for: .normal)
            user.favourite = true
        }
    }
}

public struct JSONUsers: Decodable{
    var nickName: String
    var thumbnailName: String
    var twits: [JSONTwits]
}

public struct JSONTwits: Decodable{
    var timeStamp: String
    var twit: String
}
