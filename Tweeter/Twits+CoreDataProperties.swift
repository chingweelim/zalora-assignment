//
//  Twits+CoreDataProperties.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 6/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//
//

import Foundation
import CoreData


extension Twits {

    @NSManaged public var nickName: String?
    @NSManaged public var timeStamp: NSDate?
    @NSManaged public var twit: String?
    @NSManaged public var user: Users?

}
