//
//  SingleUserViewController.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 6/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//

import UIKit

class SingleUserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var userThumbnailImageView: UIImageView!
    var singleUser: Users!
    var userTwits: [Twits]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userTwits = Twits.fetchUserTwitsRequest(nickName: singleUser.nickName!)
        navigationItem.title = singleUser.nickName!
        userThumbnailImageView.image = UIImage(named: singleUser.thumbnailName!)
    }
    
    // MARK: - TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 44.0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userTwits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "singleUserCell") as! SingleUserTwitsTableViewCell
        cell.twitLabel.text = userTwits[indexPath.row].twit
        let dateFormatter = DateFormatter()
        let creationDate = userTwits[indexPath.row].timeStamp
        dateFormatter.dateStyle = DateFormatter.Style.full
        cell.dateLabel.text = dateFormatter.string(from: creationDate! as Date)
        
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
