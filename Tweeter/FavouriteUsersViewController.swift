//
//  FavouriteUsersViewController.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 6/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//

import UIKit

class FavouriteUsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    var favouriteUsers: [Users]!
    var selectedUser: Users!
    var selectedIndexPath: IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let indexPath = selectedIndexPath {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    // MARK: - TableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 44.0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favouriteUsersCell") as! FavouriteUserTableViewCell
        let favouriteUserNickName = favouriteUsers[indexPath.row].nickName!
        let favouriteUserThumbnail = favouriteUsers[indexPath.row].thumbnailName
        cell.nickNameLabel.text = favouriteUserNickName
        cell.thumbnailImageView.image = UIImage(named: favouriteUserThumbnail!)
        cell.twitLabel.text = Twits.fetchUserTwitsRequest(nickName: favouriteUserNickName)!.first!.twit
        let dateFormatter = DateFormatter()
        let creationDate = Twits.fetchUserTwitsRequest(nickName: favouriteUserNickName)!.first!.timeStamp
        dateFormatter.dateStyle = DateFormatter.Style.full
        cell.dateLabel.text = dateFormatter.string(from: creationDate! as Date)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedUser = favouriteUsers[indexPath.row]
        selectedIndexPath = indexPath
        performSegue(withIdentifier: "FavouriteToSingleUser", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "FavouriteToSingleUser" {
            let singleUserViewController = segue.destination as! SingleUserViewController
            singleUserViewController.singleUser = selectedUser
        }
    }
    

}
