//
//  HomeUserTableViewCell.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 6/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//

import UIKit

protocol HomeCellConnectDelegate {
    
    func favouriteUnfavouriteUser(selectedQuestionButtonOutlet: UIButton!, selectedNickNamelabelOutlet: UILabel!)
    
}

class HomeUserTableViewCell: UITableViewCell {
    
    var delegate: HomeCellConnectDelegate?
    
    @IBOutlet var thumbnailImageView: UIImageView!
    @IBOutlet var nickNameLabel: UILabel!
    @IBOutlet var twitLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var favouriteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func favouriteButtonAction(_ sender: UIButton) {
        delegate?.favouriteUnfavouriteUser(selectedQuestionButtonOutlet: favouriteButton, selectedNickNamelabelOutlet: nickNameLabel)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        thumbnailImageView.image = nil
        
    }
    

}
