//
//  FavouriteUserTableViewCell.swift
//  Tweeter
//
//  Created by Lim Ching Wee on 6/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//

import UIKit

class FavouriteUserTableViewCell: UITableViewCell {

    @IBOutlet var thumbnailImageView: UIImageView!
    @IBOutlet var nickNameLabel: UILabel!
    @IBOutlet var twitLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        thumbnailImageView.image = nil
        
    }
    
}
