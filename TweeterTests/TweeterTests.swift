//
//  TweeterTests.swift
//  TweeterTests
//
//  Created by Lim Ching Wee on 2/4/19.
//  Copyright © 2019 appyeah. All rights reserved.
//

import XCTest
@testable import Tweeter

class TweeterTests: XCTestCase {

    var homeViewController: HomeViewController!
    
    override func setUp() {
        super.setUp()
        
        let sb = UIStoryboard(name:"Main", bundle: nil)
        let hvc = sb.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        homeViewController = hvc
        _ = homeViewController.view
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCharacterLimit() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(homeViewController.twitLimit(), 50)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
